<?php

use Cron\CronExpression;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Implements hook_entity_base_field_info().
 */
function unsm_mobilede_entity_base_field_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() == 'trailer') {
    $fields['mobilede_export'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Export to mobile.de'))
      ->setTranslatable(FALSE)
      ->setDefaultValue(FALSE)
      ->setSettings([
        'on_label' => new TranslatableMarkup('Yes'),
        'off_label' => new TranslatableMarkup('No'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['vin'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Vehicle Identification No.'))
      ->setSettings([
        'max_length' => 64,
      ])
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 21,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['mobilede_ad_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('mobile.de Ad ID'))
      ->setSettings([
        'max_length' => 64,
      ])
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['registration_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(new TranslatableMarkup('Date of first registration'))
      ->setDescription('Leer lassen für Neu-Anhänger.')
      ->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 23,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['mobilede_images'] = BaseFieldDefinition::create('map')
      ->setLabel(t('mobile.de image mapping'))
      ->setDescription(t('A serialized array for mobile.de image mapping.'));
  }
  return $fields;
}

/**
 * The allowed_values_function callback for 'field_mobile_de_category' field.
 *
 * Hardcoded synced on 2020-09-07.
 *
 * @see https://services.mobile.de/refdata/sites/GERMANY/classes/Trailer/categories
 */
function unsm_mobilede_allowed_mobilede_categories(FieldStorageDefinitionInterface $definition, FieldableEntityInterface $entity = NULL, &$cacheable = NULL) {
  $cacheable = TRUE;
  return [
    'BeveragesTrailer' => 'Getränke',
    'BoatTrailer' => 'Bootsanhänger',
    'BoxTrailer' => 'Koffer',
    'CarCarrierTrailer' => 'Autotransporter',
    'CattleTruckTrailer' => 'Tier-/Pferdetransport',
    'ChassisTrailer' => 'Fahrgestell',
    'ConstructionTrailer' => 'Bauwagen',
    'GlassTransportSuperstructureTrailer' => 'Glastransport',
    'HydraulicWorkPlatformTrailer' => 'Hubarbeitsbühne',
    'LongMaterialTransporterTrailer' => 'Langmaterialtransporter',
    'LowLoaderTrailer' => 'Tieflader',
    'MilkTankTrailer' => 'Lebensmitteltank',
    'MotorcycleTrailer' => 'Motorradanhänger',
    'PlatformTrailer' => 'Plattform',
    'RefrigeratorBodyTrailer' => 'Kühlkoffer',
    'RollOffTrailer' => 'Abrollanhänger',
    'SiloTrailer' => 'Silo',
    'SkipHoistTrailer' => 'Möbellift',
    'StakeBodyAndTarpaulinTrailer' => 'Pritsche + Plane',
    'StakeBodyTrailer' => 'Pritsche',
    'SwapBodyTrailer' => 'Wechselpritsche',
    'SwapChassisTrailer' => 'Wechselfahrgestell',
    'TankBodiesTrailer' => 'Tankaufbau',
    'TimberCarrierTrailer' => 'Holztransporter',
    'TipperTrailer' => 'Dreiseitenkipper',
    'TrafficConstructionTrailer' => 'Verkaufsaufbau',
    'TrailerCar' => 'Pkw-Anhänger',
    'WalkingFloorTrailer' => 'Walkingfloor',
    'OtherTrailer' => 'Andere Anhänger',
  ];
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for trailer_form.
 */
function unsm_mobilede_form_trailer_form_alter(array &$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\trailer\Form\TrailerForm $form_object */
  $form_object = $form_state->getFormObject();
  /** @var \Drupal\trailer\Entity\TrailerInterface $trailer */
  $trailer = $form_object->getEntity();
  if (!$trailer->hasField('mobilede_export')) {
    return;
  }
  $form['#validate'][] = 'unsm_mobilede_form_trailer_form_validate';
}

/**
 * Form validation callback for trailer forms with mobile.de integration.
 */
function unsm_mobilede_form_trailer_form_validate(array &$form, FormStateInterface $form_state) {
  if (empty($form_state->getValue(['mobilede_export', 'value']))) {
    return;
  }
  if (empty($form_state->getValue(['vin', 0, 'value']))) {
    $form_state->setErrorByName('vin', 'Fahrgestellnummer ist ein Pflichtfeld bei mobile.de!');
  }
  if (empty($form_state->getValue(['price', 0, 'number']))) {
    $form_state->setErrorByName('price', 'Preis ist ein Pflichtfeld bei mobile.de!');
  }
  if (empty($form_state->getValue(['category', 0, 'target_id']))) {
    \Drupal::messenger()->addWarning('Keine Kategorie (Typ) zugeordnet. Hänger wird automatisch bei mobile.de als "Andere Anhänger" klassifiziert!');
  }
}

/**
 * Implements hook_cron().
 */
function unsm_mobilede_cron() {
  $cron_interval = \Drupal::config('unsm_mobilede.settings')->get('cron_interval');
  if (empty($cron_interval) || !CronExpression::isValidExpression($cron_interval)) {
    return;
  }

  $cron_expression = new CronExpression($cron_interval);
  $due_time = $cron_expression->getPreviousRunDate()->getTimestamp();
  $state = \Drupal::state();
  $last_run = $state->get('unsm_mobilede.last_cron_export', 0);
  if (empty($last_run) || $last_run < $due_time) {
    /** @var \Drupal\unsm_mobilede\MobiledeSyncServiceInterface $mobilede_sync_service */
    $mobilede_sync_service = \Drupal::service('unsm_mobilede.mobilede_sync_service');
    // @todo should we only store the timestamp, if synchronisation was successful?
    $mobilede_sync_service->syncData();
    $state->set('unsm_mobilede.last_cron_export', \Drupal::time()->getRequestTime());
  }
}
