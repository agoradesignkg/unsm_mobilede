<?php

namespace Drupal\unsm_mobilede\Api;

use Drupal\commerce_price\Price;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\physical\LengthUnit;
use Drupal\physical\WeightUnit;
use Drupal\trailer\Entity\TrailerInterface;

/**
 * Value object representing a mobile.de ad.
 */
final class MobiledeAd {

  /**
   * The internal number - which is the VIN for exported nodes.
   *
   * @var string
   */
  protected $internalNumber;

  /**
   * The mobile ad ID.
   *
   * @var string
   */
  protected $mobileAdId;

  /**
   * The (Drupal) UUID.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The vehicle class.
   *
   * @var string
   */
  protected $vehicleClass;

  /**
   * The price.
   *
   * @var \Drupal\commerce_price\Price
   */
  protected $price;

  /**
   * The category.
   *
   * @var string
   */
  protected $category;

  /**
   * The make.
   *
   * @var string
   */
  protected $make;

  /**
   * The first registration date.
   *
   * @var \Drupal\Core\Datetime\DrupalDateTime|null
   */
  protected $registrationDate;

  /**
   * The model description.
   *
   * @var string
   */
  protected $modelDescription;

  /**
   * The VIN (vehicle identificatin number).
   *
   * Equals the internal number in most cases.
   *
   * @var string
   */
  protected $vin;

  /**
   * The licensed weight in kg.
   *
   * @var int
   */
  protected $licensedWeight;

  /**
   * The load capacity in kg.
   *
   * @var int
   */
  protected $loadCapacity;

  /**
   * An integer array having keys 'length', 'width' and 'height' (in mm).
   *
   * @var int[]
   */
  protected $loadingSpace;

  /**
   * The description.
   *
   * @var string
   */
  protected $description;

  /**
   * The images (references).
   *
   * @var \Drupal\unsm_mobilede\Api\MobiledeImage[]
   */
  protected $images;

  /**
   * Any additional fields, not having dedicated properties.
   *
   * @var array
   */
  protected $additionalData;

  /**
   * Constructs a new MobiledeAd object.
   *
   * @param string $internal_number
   *   The internal number.
   * @param string|null $mobile_ad_id
   *   The mobile ad ID.
   */
  public function __construct($internal_number, $mobile_ad_id = NULL) {
    $this->internalNumber = $internal_number;
    $this->mobileAdId = $mobile_ad_id;
    $this->vehicleClass = 'Trailer';
    $this->category = 'OtherTrailer';
    $this->make = 'UNSINN';
    $this->loadingSpace = [];
    $this->images = [];
    $this->additionalData = [];
  }

  /**
   * Factory method constructing a new object from array.
   *
   * @param array $data
   *   The data array, as returned from mobile.de Seller API.
   *
   * @return static
   *   A new MobiledeAd object.
   *
   * @throws \InvalidArgumentException
   *   Thrown, when the given array is malformed.
   */
  public static function fromArray(array $data) {
    if (!isset($data['internalNumber'], $data['mobileAdId'])) {
      throw new \InvalidArgumentException('MobiledeAd::fromArray() called with a malformed array.');
    }
    $result = new static($data['internalNumber'], $data['mobileAdId']);
    unset($data['internalNumber']);
    unset($data['mobileAdId']);
    if (isset($data['price'])) {
      $result->price = new Price($data['price']['consumerPriceGross'], $data['price']['currency']);
    }
    unset($data['price']);
    if (isset($data['firstRegistration'])) {
      $result->registrationDate = DrupalDateTime::createFromFormat('Ym', $data['firstRegistration']);
    }
    unset($data['firstRegistration']);
    $attrs = [
      'vehicleClass',
      'category',
      'make',
      'modelDescription',
      'vin',
      'licensedWeight',
      'loadCapacity',
      'loadingSpace',
      'description',
    ];
    foreach ($attrs as $attr) {
      if (isset($data[$attr])) {
        $result->{$attr} = $data[$attr];
        unset($data[$attr]);
      }
    }
    if (!empty($data['images'])) {
      $images = [];
      foreach ($data['images'] as $image) {
        $images[] = MobiledeImage::fromArray($image);
      }
      $result->setImages($images);
      unset($data['images']);
    }
    // Store everything that's still inside $data in additional data field.
    $result->additionalData = $data;
    return $result;
  }

  /**
   * Factory method constructing a new object from a trailer entity.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   *
   * @return static
   *   A new MobiledeAd object.
   */
  public static function fromTrailer(TrailerInterface $trailer) {
    $vin = static::safeGetValue($trailer, 'vin');
    $vin = mb_strtoupper($vin);
    $internal_number = $vin ?: $trailer->uuid();
    $mobilede_ad_id = static::safeGetValue($trailer, 'mobilede_ad_id');
    $result = new static($internal_number, $mobilede_ad_id);
    $result->vin = $vin;
    $result->setUuid($trailer->uuid());
    $result->modelDescription = $trailer->label();
    $result->price = $trailer->getPrice();
    if ($category = $trailer->getCategory()) {
      if ($category->hasField('field_mobile_de_category') && !$category->get('field_mobile_de_category')->isEmpty()) {
        $result->category = $category->field_mobile_de_category->value;
      }
    }
    if ($trailer->hasField('registration_date') && !empty($trailer->get('registration_date'))) {
      /** @var \Drupal\datetime\Plugin\Field\FieldType\DateTimeItem $date_field_item */
      $date_field_item = $trailer->get('registration_date')->first();
      $result->registrationDate = new DrupalDateTime($date_field_item->value);
    }
    if ($weight = $trailer->getTotalWeight()) {
      $weight = $weight->convert(WeightUnit::KILOGRAM);
      $result->licensedWeight = (int) $weight->getNumber();
    }
    if ($load_capacity = $trailer->getLoadCapacity()) {
      $load_capacity = $load_capacity->convert(WeightUnit::KILOGRAM);
      $result->loadCapacity = (int) $load_capacity->getNumber();
    }

    $length = $trailer->getInternalLength();
    $width = $trailer->getInternalWidth();
    $height = $trailer->getInternalHeight();
    if ($length && $width && $height) {
      $length = $length->convert(LengthUnit::MILLIMETER);
      $width = $width->convert(LengthUnit::MILLIMETER);
      $height = $height->convert(LengthUnit::MILLIMETER);
      $result->loadingSpace = [
        'length' => (int) $length->getNumber(),
        'width' => (int) $width->getNumber(),
        'height' => (int) $height->getNumber(),
      ];
    }
    if ($description = $trailer->getDescription()) {
      $description = strip_tags($description);
      $result->description = $description;
    }

    return $result;
  }

  /**
   * Returns an array representation of the object.
   *
   * @return array
   *   The data as array, compatible with mobile.de Seller API calls.
   */
  public function toArray() {
    $result = $this->additionalData ?: [];
    $result['internalNumber'] = $this->getInternalNumber();
    $result['mobileAdId'] = $this->getMobileAdId();
    $result['vehicleClass'] = $this->vehicleClass;
    if ($this->price) {
      $result['price'] = [
        'consumerPriceGross' => $this->price->getNumber(),
        'currency' => $this->price->getCurrencyCode(),
      ];
    }
    $result['category'] = $this->category;
    $result['make'] = $this->make;
    if ($this->registrationDate) {
      $result['firstRegistration'] = $this->registrationDate->format('Ym');
      $result['condition'] = 'USED';
    }
    else {
      $result['condition'] = 'NEW';
    }
    $result['modelDescription'] = $this->modelDescription;
    $result['vin'] = $this->vin;
    if ($this->licensedWeight) {
      $result['licensedWeight'] = $this->licensedWeight;
    }
    if ($this->loadCapacity) {
      $result['loadCapacity'] = $this->loadCapacity;
    }
    if ($this->loadingSpace) {
      $result['loadingSpace'] = $this->loadingSpace;
    }
    if ($this->description) {
      $result['description'] = $this->description;
    }
    if ($this->images) {
      $result['images'] = [];
      foreach ($this->images as $image) {
        $result['images'][] = $image->toArray();
      }
    }
    return $result;
  }

  /**
   * Get the internal number
   *
   * @return string
   *   The internal number.
   */
  public function getInternalNumber() {
    return $this->internalNumber;
  }

  /**
   * Get the mobile ad ID.
   *
   * @return string
   *   The mobile ad ID.
   */
  public function getMobileAdId() {
    return $this->mobileAdId;
  }

  /**
   * Sets the mobile.de ad ID.
   *
   * @param string $mobilede_ad_id
   *   The mobile ad ID.
   */
  public function setMobiledeAdId($mobilede_ad_id) {
    $this->mobileAdId = $mobilede_ad_id;
  }

  /**
   * Returns whether the mobile.de ad is new (not existing yet).
   *
   * Please note, that it is only tested, whether the mobileAdId property is
   * set at all.
   *
   * @return bool
   *   TRUE, if the mobile.de ad is not yet existing, FALSE otherwise.
   */
  public function isNew() {
    return empty($this->mobileAdId);
  }

  /**
   * Unsets the mobile.de ad ID in case it is was deleted remotely.
   */
  public function unsetMobiledeAdId() {
    $this->mobileAdId = NULL;
  }

  /**
   * Get the (Drupal) UUID.
   *
   * @return string
   *   The (Drupal) UUID.
   */
  public function getUuid() {
    return $this->uuid;
  }

  /**
   * Set the (Drupal) UUID.
   *
   * @param string $uuid
   *   The (Drupal) UUID.
   */
  public function setUuid($uuid) {
    $this->uuid = $uuid;
  }

  /**
   * Get the images.
   *
   * @return \Drupal\unsm_mobilede\Api\MobiledeImage[]
   *   The images.
   */
  public function getImages() {
    return $this->images;
  }

  /**
   * Set the images.
   *
   * @param \Drupal\unsm_mobilede\Api\MobiledeImage[] $images
   *   The images.
   */
  public function setImages($images) {
    $this->images = $images;
  }

  /**
   * Get the additional data.
   *
   * @return array
   *   The additional data.
   */
  public function getAdditionalData() {
    return $this->additionalData;
  }

  /**
   * Set the additional data.
   *
   * @param array $additional_data
   *   The additional data.
   */
  public function setAdditionalData(array $additional_data) {
    $this->additionalData = $additional_data;
  }

  /**
   * Checks whether or not the given array equals this object.
   *
   * The comparison is meant for comparing different versions of the same ad, in
   * order to find out, whether or not we need to update.
   *
   * @param \Drupal\unsm_mobilede\Api\MobiledeAd $another_ad
   *   Another ad to compare.
   *
   * @return bool
   *   TRUE, if the two ad objects equal each other, FALSE otherwise.
   */
  public function equals(MobiledeAd $another_ad) {
    $equals = $this->toArray() == $another_ad->toArray();
    if (!$equals) {
      return FALSE;
    }
    if ($this->images || $another_ad->images) {
      $equals = count($this->images) === count($another_ad->getImages());
      if (!$equals) {
        return FALSE;
      }
      foreach ($this->images as $idx => $image) {
        $equals = $another_ad->getImages()[$idx]->toArray() == $image->toArray();
        if (!$equals) {
          return FALSE;
        }
      }
    }
    return $this->getAdditionalData() == $another_ad->getAdditionalData();
  }

  /**
   * Convenience helper for safe getting a field value from the given trailer.
   *
   * @param \Drupal\trailer\Entity\TrailerInterface $trailer
   *   The trailer entity.
   * @param string $field_name
   *   The field name.
   *
   * @return string|NULL
   */
  protected static function safeGetValue(TrailerInterface $trailer, $field_name) {
    return $trailer->hasField($field_name) && !$trailer->get($field_name)->isEmpty() ?
      $trailer->{$field_name}->value : NULL;
  }

}
