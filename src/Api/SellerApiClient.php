<?php

namespace Drupal\unsm_mobilede\Api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;
use Drupal\unsm_mobilede\Api\Exception\MobiledeFailedRequestException;
use Drupal\unsm_mobilede\Api\Exception\MobiledeIncompleteCredentialsException;
use Drupal\unsm_mobilede\Api\Exception\MobiledeInvalidResponseException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;

/**
 * Default mobile.de Seller API client implementation.
 */
class SellerApiClient implements SellerApiClientInterface {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The endpoint host url.
   *
   * @var string
   */
  protected $host;

  /**
   * The mobile.de seller ID.
   *
   * @var string
   */
  protected $sellerId;

  /**
   * The mobile.de username.
   *
   * @var string
   */
  protected $username;

  /**
   * The mobile.de password.
   *
   * @var string
   */
  protected $password;

  /**
   * The image style to use for photo upload.
   *
   * @var \Drupal\image\ImageStyleInterface|null
   */
  protected $imageStyle;

  /**
   * Constructs a SellerApiClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param string $seller_id
   *   The mobile.de seller ID.
   * @param string $username
   *   The username.
   * @param string $password
   *   The password.
   * @param string $host
   *   The endpoint host url.
   */
  public function __construct(ClientInterface $http_client, $seller_id, $username, $password, $host) {
    $this->httpClient = $http_client;
    $this->sellerId = $seller_id;
    $this->username = $username;
    $this->password = $password;
    $this->host = $host;
  }

  /**
   * Set the image style.
   *
   * @param \Drupal\image\ImageStyleInterface|null $imageStyle
   *   The image style.
   */
  public function setImageStyle($imageStyle) {
    $this->imageStyle = $imageStyle;
  }

  /**
   * {@inheritdoc}
   */
  public function loadExistingAds($index_by_internal_number = FALSE) {
    $target_url = sprintf('%s/seller-api/sellers/%s/ads', $this->host, $this->sellerId);
    $options = $this->getCommonRequestOptions(FALSE);
    try {
      $response = $this->httpClient->request('GET', $target_url, $options);
    }
    catch (RequestException $ex) {
      throw new MobiledeFailedRequestException($ex->getMessage(), $ex->getCode(), $ex);
    }
    if ($response->getStatusCode() !== 200) {
      throw new MobiledeFailedRequestException($this->t('Failed to load existing mobile.de ads.'), $response->getStatusCode());
    }

    $data = (string) $response->getBody();
    $data = Json::decode($data);
    if (!is_array($data)) {
      throw new MobiledeInvalidResponseException($this->t('Invalid JSON response from @url', ['@url' => $target_url]));
    }

    $result = [];
    if (!empty($data['ads'])) {
      foreach ($data['ads'] as $ad_data) {
        if (!isset($ad_data['internalNumber'])) {
          // Skip ads that do not have an internal number (manually created ad).
          continue;
        }
        $ad = MobiledeAd::fromArray($ad_data);
        $index_key = $index_by_internal_number ? $ad->getInternalNumber() : $ad->getMobileAdId();
        $result[$index_key] = $ad;
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function updateAd(MobiledeAd $ad) {
    $target_url = sprintf('%s/seller-api/sellers/%s/ads/%s', $this->host, $this->sellerId, $ad->getMobileAdId());
    $options = $this->getCommonRequestOptions(TRUE);
    $options[RequestOptions::JSON] = $ad->toArray();
    try {
      $response = $this->httpClient->request('PUT', $target_url, $options);
    }
    catch (RequestException $ex) {
      throw new MobiledeFailedRequestException($ex->getMessage(), $ex->getCode(), $ex);
    }
    if ($response->getStatusCode() !== 200) {
      throw new MobiledeFailedRequestException($this->t('Failed to update ad ID ' . $ad->getMobileAdId()), $response->getStatusCode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAd($mobile_ad_id) {
    $target_url = sprintf('%s/seller-api/sellers/%s/ads/%s', $this->host, $this->sellerId, $mobile_ad_id);
    $options = $this->getCommonRequestOptions(FALSE);
    try {
      $response = $this->httpClient->request('DELETE', $target_url, $options);
    }
    catch (RequestException $ex) {
      throw new MobiledeFailedRequestException($ex->getMessage(), $ex->getCode(), $ex);
    }
    if ($response->getStatusCode() !== 200) {
      throw new MobiledeFailedRequestException($this->t('Failed to delete ad ID ' . $mobile_ad_id), $response->getStatusCode());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createAd(MobiledeAd $ad) {
    $target_url = sprintf('%s/seller-api/sellers/%s/ads', $this->host, $this->sellerId);
    $options = $this->getCommonRequestOptions(TRUE);
    $options[RequestOptions::HEADERS]['X-Mobile-Insertion-Request-Id'] = $ad->getUuid();
    $options[RequestOptions::JSON] = $ad->toArray();
    try {
      $response = $this->httpClient->request('POST', $target_url, $options);
    }
    catch (RequestException $ex) {
      throw new MobiledeFailedRequestException($ex->getMessage(), $ex->getCode(), $ex);
    }
    if ($response->getStatusCode() !== 201) {
      // @todo handle 303 response!
      throw new MobiledeFailedRequestException($this->t('Failed to load seller information.'), $response->getStatusCode());
    }
    $location = $response->getHeaderLine('location');
    $location_parts = explode('/', $location);
    $mobile_ad_id = end($location_parts);
    $ad->setMobiledeAdId($mobile_ad_id);
    return $ad;
  }

  /**
   * {@inheritdoc}
   */
  public function uploadImage(FileInterface $image) {
    $image_uri = $image->getFileUri();
    if ($this->imageStyle) {
      $original_uri = $image_uri;
      $image_uri = $this->imageStyle->buildUri($image_uri);
      if (!file_exists($image_uri)) {
        $this->imageStyle->createDerivative($original_uri, $image_uri);
      }
    }
    $target_url = sprintf('%s/seller-api/images', $this->host);
    $options = $this->getCommonRequestOptions(FALSE);
    $options[RequestOptions::HEADERS]['Content-Type'] = 'image/jpeg';
    $options[RequestOptions::BODY] = file_get_contents($image_uri);
    try {
      $response = $this->httpClient->request('POST', $target_url, $options);
    }
    catch (RequestException $ex) {
      throw new MobiledeFailedRequestException($ex->getMessage(), $ex->getCode(), $ex);
    }
    if ($response->getStatusCode() !== 201) {
      throw new MobiledeFailedRequestException($this->t('Failed to upload image ' . $image_uri), $response->getStatusCode());
    }

    $data = (string) $response->getBody();
    $data = Json::decode($data);
    if (!is_array($data) || !isset($data['ref'])) {
      throw new MobiledeInvalidResponseException($this->t('Invalid JSON response from @url', ['@url' => $target_url]));
    }
    return MobiledeImage::fromArray($data);
  }

  /**
   * {@inheritdoc}
   */
  public function getSellerInfo() {
    $target_url = sprintf('%s/seller-api/sellers/%s', $this->host, $this->sellerId);
    $options = $this->getCommonRequestOptions(FALSE);
    try {
      $response = $this->httpClient->request('GET', $target_url, $options);
    }
    catch (RequestException $ex) {
      throw new MobiledeFailedRequestException($ex->getMessage(), $ex->getCode(), $ex);
    }
    if ($response->getStatusCode() !== 200) {
      throw new MobiledeFailedRequestException($this->t('Failed to load seller information.'), $response->getStatusCode());
    }

    $data = (string) $response->getBody();
    $data = Json::decode($data);
    if (!is_array($data)) {
      throw new MobiledeInvalidResponseException($this->t('Invalid JSON response from @url', ['@url' => $target_url]));
    }
    return $data;
  }

  /**
   * Checks, if complete login credentials are available.
   *
   * This does not check, if the credentials are valid. It only checks for
   * emptiness, because missing credentials can never be valid.
   *
   * @param bool $check_seller_id
   *   Whether or not to check the seller ID as well. Defaults to TRUE.
   *
   * @return bool
   *   TRUE, if we have host, username, password and (optionally) seller ID set
   *   in our config data.
   */
  protected function hasCompleteLoginCredentials($check_seller_id = TRUE) {
    $credentials_check = !empty($this->host) && !empty($this->username) && !empty($this->password);
    $seller_id_check = $check_seller_id ? !empty($this->sellerId) : TRUE;
    return $credentials_check && $seller_id_check;
  }

  /**
   * Gets common request options needed for any request sent to mobile.de
   *
   * @param bool $is_write_operation
   *   Whether the options are used for a write operation. In this case, the
   *   content type will be set accordingly.
   *
   * @return array|\string[][]
   *
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeIncompleteCredentialsException
   *   If the provided login credentials are incomplete.
   */
  protected function getCommonRequestOptions($is_write_operation) {
    if (!$this->hasCompleteLoginCredentials()) {
      throw new MobiledeIncompleteCredentialsException($this->t('mobile.de Seller API connection data incomplete! Please check configuration!'));
    }

    $options = [
      RequestOptions::AUTH => [$this->username, $this->password],
      RequestOptions::HEADERS => [
        'Accept' => 'application/vnd.de.mobile.api+json',
      ],
    ];

    if ($is_write_operation) {
      $options[RequestOptions::HEADERS]['Content-Type'] = 'application/vnd.de.mobile.api+json';
    }
    return $options;
  }

}
