<?php

namespace Drupal\unsm_mobilede\Api\Exception;

/**
 * Defines the mobile.de incomplete credentials exception.
 */
class MobiledeIncompleteCredentialsException extends MobiledeException {

}
