<?php

namespace Drupal\unsm_mobilede\Api\Exception;

/**
 * Defines a dedicated exception class for mobile.de requests.
 */
class MobiledeException extends \RuntimeException {

}
