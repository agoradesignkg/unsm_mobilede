<?php

namespace Drupal\unsm_mobilede\Api\Exception;

/**
 * Defines the mobile.de failed request exception.
 *
 * This will be thrown by the Seller API client, if a request did not respond
 * with a HTTP 200 status code.
 */
class MobiledeFailedRequestException extends MobiledeException {

}
