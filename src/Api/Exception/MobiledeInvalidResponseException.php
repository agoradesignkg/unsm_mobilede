<?php

namespace Drupal\unsm_mobilede\Api\Exception;

/**
 * Defines the mobile.de invalid response exception.
 *
 * This will be thrown by the Seller API client, if the response is not in the
 * expected format (JSON).
 */
class MobiledeInvalidResponseException extends MobiledeException {

}
