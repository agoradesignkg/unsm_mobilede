<?php

namespace Drupal\unsm_mobilede\Api;

/**
 * Defines the mobile.de image value object.
 */
final class MobiledeImage {

  /**
   * The image reference.
   *
   * @var string
   */
  protected $ref;

  /**
   * The base url.
   *
   * @var string
   */
  protected $baseUrl;

  /**
   * The hash.
   *
   * @var string
   */
  protected $hash;

  /**
   * Constructs a new MobiledeImage object.
   *
   * @param string $ref
   *   The image reference.
   * @param string|null $base_url
   *   The base url.
   * @param string|null $hash
   *   The hash.
   */
  public function __construct($ref, $base_url = NULL, $hash = NULL) {
    $this->ref = $ref;
    $this->baseUrl = $base_url;
    $this->hash = $hash;
  }

  /**
   * Returns an array representation of the object.
   *
   * @return array
   *   The data as array, compatible with mobile.de Seller API calls.
   */
  public function toArray() {
    return [
      'baseUrl' => $this->baseUrl,
      'ref' => $this->ref,
      'hash' => $this->hash,
    ];
  }

  /**
   * Factory method constructing a new object from array.
   *
   * @param array $data
   *   The data array, as returned from mobile.de Seller API.
   *
   * @return static
   *   A new MobiledeImage object.
   *
   * @throws \InvalidArgumentException
   *   Thrown, when the given array is malformed.
   */
  public static function fromArray(array $data) {
    if (!isset($data['ref'])) {
      throw new \InvalidArgumentException('MobiledeImage::fromArray() called with a malformed array.');
    }
    return new static($data['ref'], $data['baseUrl'], $data['hash']);
  }

  /**
   * Get the ref.
   *
   * @return string
   *   The ref.
   */
  public function getRef() {
    return $this->ref;
  }

}
