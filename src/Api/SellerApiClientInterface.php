<?php

namespace Drupal\unsm_mobilede\Api;

use Drupal\file\FileInterface;

/**
 * Defines the interface for connecting to mobile.de Seller API.
 */
interface SellerApiClientInterface {

  /**
   * Returns the existing mobile.de ads.
   *
   * @param bool $index_by_internal_number
   *   Whether or not to index by internal number. If FALSE, the array will be
   *   indexed by mobile ad ID. Defaults to FALSE.
   *
   * @return \Drupal\unsm_mobilede\Api\MobiledeAd[]
   *   An array of existing ads.
   *
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeFailedRequestException
   *   If the returned response does not have set HTTP 200 status.
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeIncompleteCredentialsException
   *   If the provided login credentials are incomplete.
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeInvalidResponseException
   *   If the returned response is not in the expected format (JSON).
   */
  public function loadExistingAds($index_by_internal_number = FALSE);

  /**
   * Update an existing ad.
   *
   * @param \Drupal\unsm_mobilede\Api\MobiledeAd $ad
   *   The mobile.de ad.
   *
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeFailedRequestException
   *   If the returned response does not have set HTTP 200 status.
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeIncompleteCredentialsException
   *   If the provided login credentials are incomplete.
   */
  public function updateAd(MobiledeAd $ad);

  /**
   * Creates a new ad.
   *
   * @param \Drupal\unsm_mobilede\Api\MobiledeAd $ad
   *   The mobile.de ad to create.
   *
   * @return \Drupal\unsm_mobilede\Api\MobiledeAd
   *   The mobile.de ad. We haven't implemented handling of HTTP 303 responses
   *   yet, but these can return a completely new ad object instead, once
   *   implemented.
   *
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeFailedRequestException
   *   If the returned response does not have set HTTP 201 status.
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeIncompleteCredentialsException
   *   If the provided login credentials are incomplete.
   */
  public function createAd(MobiledeAd $ad);

  /**
   * Delete an existing ad.
   *
   * @param string $mobile_ad_id
   *   The mobile.de ad ID to delete.
   *
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeFailedRequestException
   *   If the returned response does not have set HTTP 200 status.
   * @throws \Drupal\unsm_mobilede\Api\Exception\MobiledeIncompleteCredentialsException
   *   If the provided login credentials are incomplete.
   */
  public function deleteAd($mobile_ad_id);

  /**
   * Uploads an image.
   *
   * @param \Drupal\file\FileInterface $image
   *   The image file.
   *
   * @return \Drupal\unsm_mobilede\Api\MobiledeImage|null
   *   The image reference. NULL, if an error occurred.
   */
  public function uploadImage(FileInterface $image);

  /**
   * Queries information about the seller account.
   *
   * @return array
   *   The seller account information as returned by mobile.de Seller API.
   */
  public function getSellerInfo();

}
