<?php

namespace Drupal\unsm_mobilede;

/**
 * Defines the mobile.de sync service interface.
 */
interface MobiledeSyncServiceInterface {

  /**
   * Syncs mobile.de data by creating new ads and deleting obsolete ones.
   *
   * @return bool
   *   TRUE, if sync was successful. FALSE otherwise on any error occured.
   */
  public function syncData();

  /**
   * Loads the trailers that should be exported to mobile.de.
   *
   * @return \Drupal\trailer\Entity\TrailerInterface[]
   *   An array of trailers that should be exported to mobile.de.
   */
  public function loadTrailersToExport();

  /**
   * Queries information about the seller account.
   *
   * @return array
   *   The seller account information as returned by mobile.de Seller API.
   */
  public function getSellerInfo();

}
