<?php

namespace Drupal\unsm_mobilede;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\unsm_mobilede\Api\Exception\MobiledeException;
use Drupal\unsm_mobilede\Api\MobiledeAd;
use Drupal\unsm_mobilede\Api\MobiledeImage;
use Drupal\unsm_mobilede\Api\SellerApiClient;
use GuzzleHttp\ClientInterface;

/**
 * Default mobile.de sync service implementation.
 */
class MobiledeSyncService implements MobiledeSyncServiceInterface {

  /**
   * The module's setting configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   *   The logger.
   */
  protected $logger;

  /**
   * The mobile.de Seller API client.
   *
   * @var \Drupal\unsm_mobilede\Api\SellerApiClientInterface
   */
  protected $sellerApiClient;

  /**
   * The image style storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The trailer storage.
   *
   * @var \Drupal\trailer\TrailerStorageInterface
   */
  protected $trailerStorage;

  /**
   * Constructs a MobiledeSyncService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @oaram \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->config = $config_factory->get('unsm_mobilede.settings');
    $this->httpClient = $http_client;
    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
    $this->logger = $logger_channel_factory->get('unsm_mobilede');
    $this->trailerStorage = $entity_type_manager->getStorage('trailer');
    $seller_id = $this->config->get('seller_id');
    $username = $this->config->get('username');
    $password = $this->config->get('password');
    $mode = $this->config->get('mode');
    $host = $mode === 'live' ? $this->config->get('host_live') : $this->config->get('host_test');
    $this->sellerApiClient = new SellerApiClient($this->httpClient, $seller_id, $username, $password, $host);
    $image_style_name = $this->config->get('image_style');
    if ($image_style_name) {
      /** @var \Drupal\image\ImageStyleInterface|null $image_style */
      $image_style = $this->imageStyleStorage->load($image_style_name);
      if ($image_style) {
        $this->sellerApiClient->setImageStyle($image_style);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncData() {
    $trailers = $this->loadTrailersToExport();
    if (!empty($trailers)) {
      try {
        $existing_ads = $this->sellerApiClient->loadExistingAds();
      }
      catch (MobiledeException $ex) {
        $this->logger->error($ex->getMessage() ?: 'Exception occurred on syncing mobile.de data.');
        return FALSE;
      }

      $created = 0;
      $updated = 0;
      $deleted = 0;
      $errors = 0;
      $skipped = 0;
      foreach ($trailers as $trailer) {
        $ad = MobiledeAd::fromTrailer($trailer);
        if (!$ad->isNew() && !isset($existing_ads[$ad->getMobileAdId()])) {
          $ad->unsetMobiledeAdId();
        }

        if ($ad->isNew()) {
          if ($trailer->hasField('field_images') && !$trailer->get('field_images')->isEmpty()) {
            /** @var \Drupal\file\FileInterface[] $images */
            $images = $trailer->get('field_images')->referencedEntities();
            $uploaded_images = [];
            $mobilede_image_refs = [];
            foreach ($images as $image) {
              try {
                $uploaded_images[$image->id()] = $this->sellerApiClient->uploadImage($image);
              }
              catch (MobiledeException $ex) {
                $this->logger->error($ex->getMessage() ?: 'Exception occurred on uploading image for trailer ID ' . $trailer->id());
              }
            }
            if (!empty($uploaded_images)) {
              $mobilede_image_refs = [];
              $remote_images = [];
              foreach($uploaded_images as $file_id => $uploaded_image) {
                $mobilede_image_refs['file:' . $file_id] = $uploaded_image->toArray();
                $remote_images[] = $uploaded_image;
              }
              $ad->setImages($remote_images);
            }
          }
          $this->sellerApiClient->createAd($ad);
          $trailer->set('mobilede_ad_id', $ad->getMobileAdId());
          if (!empty($mobilede_image_refs)) {
            $trailer->set('mobilede_images', $mobilede_image_refs);
          }
          $trailer->save();
          $created++;
        }
        else {
          $remote_version = $existing_ads[$ad->getMobileAdId()];
          $ad->setImages($remote_version->getImages());
          $ad->setAdditionalData($remote_version->getAdditionalData());

          $has_images = $trailer->hasField('field_images') && !$trailer->get('field_images')->isEmpty();
          $has_stored_remote_images_info = $trailer->hasField('mobilede_images') && !$trailer->get('mobilede_images')->isEmpty();
          $trailer_entity_needs_save = FALSE;
          if ($has_images || $has_stored_remote_images_info) {
            if (!$has_images) {
              $ad->setImages([]);
              $trailer->set('mobilede_images', NULL);
              $trailer_entity_needs_save = TRUE;
            }
            else {
              $current_fids = [];
              $stored_mapped_fids = [];
              /** @var \Drupal\file\FileInterface $file */
              foreach ($trailer->get('field_images')->referencedEntities() as $file) {
                $current_fids[] = 'file:' . $file->id();
              }
              $mobilede_images = $trailer->get('mobilede_images')->first()->getValue();
              if (is_array($mobilede_images)) {
                $stored_mapped_fids = array_keys($mobilede_images);
              }
              if ($stored_mapped_fids != $current_fids) {
                $remote_images = [];
                $remote_images_info = [];
                foreach ($trailer->get('field_images')->referencedEntities() as $file) {
                  $file_identifier = 'file:' . $file->id();
                  $existing_remote_image = NULL;
                  if (isset($mobilede_images[$file_identifier])) {
                    $stored_remote_image_info = MobiledeImage::fromArray($mobilede_images[$file_identifier]);
                    foreach ($ad->getImages() as $image) {
                      if ($image->getRef() == $stored_remote_image_info->getRef()) {
                        $existing_remote_image = $image;
                        $remote_images[] = $existing_remote_image;
                        $remote_images_info[$file_identifier] = $mobilede_images[$file_identifier];
                        break;
                      }
                    }
                  }
                  if (empty($existing_remote_image)) {
                    try {
                      $uploaded_image = $this->sellerApiClient->uploadImage($file);
                      $remote_images[] = $uploaded_image;
                      $remote_images_info[$file_identifier] = $uploaded_image->toArray();
                    }
                    catch (MobiledeException $ex) {
                      $this->logger->error($ex->getMessage() ?: 'Exception occurred on uploading image for trailer ID ' . $trailer->id());
                    }
                  }
                }
                $ad->setImages($remote_images);
                $trailer->set('mobilede_images', $remote_images_info);
                $trailer_entity_needs_save = TRUE;
              }
            }
            if ($trailer_entity_needs_save) {
              $trailer->save();
            }
          }

          if ($ad->equals($remote_version)) {
            $skipped++;
            unset($existing_ads[$ad->getMobileAdId()]);
            continue;
          }

          try {
            $this->sellerApiClient->updateAd($ad);
            $updated++;
          }
          catch (MobiledeException $ex) {
            $this->logger->error($ex->getMessage() ?: 'Exception occurred on updating mobile.de ad for trailer ID ' . $trailer->id());
            $errors++;
          }

          // Remove from existing ads array, so we can do a final cleanup later.
          unset($existing_ads[$ad->getMobileAdId()]);
        }
      }

      foreach ($existing_ads as $deprecated_ad) {
        try {
          $this->sellerApiClient->deleteAd($deprecated_ad->getMobileAdId());
          $deleted++;
        }
        catch (MobiledeException $ex) {
          $this->logger->error($ex->getMessage() ?: 'Exception occurred on deleting mobile.de ad ID ' . $deprecated_ad->getMobileAdId());
          $errors++;
        }
      }
      $this->logger->info('Finished mobile.de sync: created %created, updated %updated, skipped %skipped, deleted %deleted; %errors errors.', [
        '%created' => $created,
        '%updated' => $updated,
        '%skipped' => $skipped,
        '%deleted' => $deleted,
        '%errors' => $errors,
      ]);
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function loadTrailersToExport() {
    /** @var \Drupal\trailer\Entity\TrailerInterface[] $trailers */
    $trailers = $this->trailerStorage->loadByProperties([
      'status' => 1,
      'mobilede_export' => 1,
    ]);
    return $trailers;
  }

  /**
   * {@inheritdoc}
   */
  public function getSellerInfo() {
    return $this->sellerApiClient->getSellerInfo();
  }

}
