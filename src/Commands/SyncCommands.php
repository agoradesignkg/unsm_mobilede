<?php

namespace Drupal\unsm_mobilede\Commands;

use Drupal\unsm_mobilede\MobiledeSyncServiceInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands for mobile.de synchronisation.
 */
class SyncCommands extends DrushCommands {

  /**
   * The mobile.de sync service.
   *
   * @var \Drupal\unsm_mobilede\MobiledeSyncServiceInterface
   */
  protected $mobiledeSyncService;

  /**
   * Constructs a new SyncCommands object.
   *
   * @param MobiledeSyncServiceInterface $mobilede_sync_service
   *   The mobile.de sync service.
   */
  public function __construct(MobiledeSyncServiceInterface $mobilede_sync_service) {
    parent::__construct();

    $this->mobiledeSyncService = $mobilede_sync_service;
  }

  /**
   * Prints information about the seller account.
   *
   * @command unsm_mobilede:seller-info
   *
   * @usage unsm_mobilede:seller-info
   *   Prints information about the seller account.
   *
   * @validate-module-enabled unsm_mobilede
   */
  public function printSellerInfo() {
    $seller_info = $this->mobiledeSyncService->getSellerInfo();
    $this->writeln(print_r($seller_info, TRUE));
  }

  /**
   * Runs full synchronization.
   *
   * @command unsm_mobilede:sync
   *
   * @usage unsm_mobilede:sync
   *   Runs full synchronization.
   *
   * @validate-module-enabled unsm_mobilede
   */
  public function runSync() {
    $success = $this->mobiledeSyncService->syncData();
    if ($success) {
      $this->writeln('mobile.de synchronisation finished successfully!');
    }
    else {
      $this->writeln('mobile.de synchronisation failed!');
    }
  }

}
